import { IsEnum } from 'class-validator';
import { TodoStatus } from '../enum/todoStatus.enum';
import { CreateTodoDto } from './createTodo.dto';

export class UpdateTodoDto extends CreateTodoDto {
  @IsEnum(TodoStatus)
  status: TodoStatus;
}
