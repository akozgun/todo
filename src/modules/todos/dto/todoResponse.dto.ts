import { UpdateTodoDto } from './updateTodo.dto';

export class TodoResponseDto extends UpdateTodoDto {
  id: string;
  dateCreated: Date;
  dateLastUpdated: Date;
}
