export enum TodoStatus {
  NOT_STARTED,
  IN_PROGRESS,
  DONE,
}
