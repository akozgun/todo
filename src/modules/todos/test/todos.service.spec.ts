import { Test, TestingModule } from '@nestjs/testing';
import { TodoRepository } from '../repository/todo.repository';
import { TodosService } from '../todos.service';

describe('TodosService', () => {
  let service: TodosService;
  const mockTodoRepository = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [TodosService, TodoRepository],
    })
      .overrideProvider(TodoRepository)
      .useValue(mockTodoRepository)
      .compile();

    service = module.get<TodosService>(TodosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
