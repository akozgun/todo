import { Test, TestingModule } from '@nestjs/testing';
import { TodoStatus } from '../enum/todoStatus.enum';
import { TodosController } from '../todos.controller';
import { TodosService } from '../todos.service';
import { v4 as uuid } from 'uuid';

describe('TodosController', () => {
  let sut: TodosController;

  const mockTodoService = {
    create: jest.fn((dto) => {
      return {
        ...dto,
        id: uuid(),
        dateCreated: new Date(),
        dateLastUpdated: new Date(),
        status: TodoStatus.NOT_STARTED,
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [TodosController],
      providers: [TodosService],
    })
      .overrideProvider(TodosService)
      .useValue(mockTodoService)
      .compile();

    sut = module.get<TodosController>(TodosController);
  });

  it('should be defined', () => {
    expect(sut).toBeDefined();
  });

  describe('Create function', () => {
    it('should create a todo item', async () => {
      const todoName = 'finish writing unit tests';
      const data = { name: todoName };

      const expected = {
        id: expect.any(String),
        name: todoName,
        dateCreated: expect.any(Date),
        dateLastUpdated: expect.any(Date),
        status: TodoStatus.NOT_STARTED,
      };

      expect(await sut.create(data)).toStrictEqual(expected);
      expect(mockTodoService.create).toBeCalledTimes(1);
    });
  });
});
