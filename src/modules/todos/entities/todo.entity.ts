import { AuditableEntity } from '../../../core/entity/auditable.entity';
import { Column, Entity } from 'typeorm';
import { TodoStatus } from '../enum/todoStatus.enum';

@Entity()
export class Todo extends AuditableEntity {
  @Column({ type: 'varchar' })
  name: string;
  @Column({ type: 'integer', default: TodoStatus.NOT_STARTED })
  status: TodoStatus;
}
