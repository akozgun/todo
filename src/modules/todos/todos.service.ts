import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTodoDto } from './dto/createTodo.dto';
import { TodoResponseDto } from './dto/todoResponse.dto';
import { UpdateTodoDto } from './dto/updateTodo.dto';
import { TodoRepository } from './repository/todo.repository';
@Injectable()
export class TodosService {
  constructor(private todoRepository: TodoRepository) {}

  async create(createTodoDto: CreateTodoDto): Promise<TodoResponseDto> {
    return this.todoRepository.save(createTodoDto);
  }

  async findAll(): Promise<TodoResponseDto[]> {
    return this.todoRepository.find({
      where: { isDeleted: false },
      select: ['id', 'name', 'dateCreated', 'dateLastUpdated', 'status'],
    });
  }

  async findOne(id: string): Promise<TodoResponseDto> {
    const result = await this.todoRepository.findOne({
      where: { id, isDeleted: false },
      select: ['id', 'name', 'dateCreated', 'dateLastUpdated', 'status'],
    });

    if (!result)
      throw new NotFoundException(`Todo item with the ID ${id} is not found.`);

    return result;
  }

  async update(
    id: string,
    updateTodoDto: UpdateTodoDto,
  ): Promise<TodoResponseDto> {
    const todo = await this.findOne(id);
    await this.todoRepository.update(id, updateTodoDto);
    return { ...todo, ...updateTodoDto };
  }

  async remove(id: string) {
    await this.findOne(id);
    const result = await this.todoRepository.update(id, { isDeleted: true });
    return { result: result && result.affected == 1 };
  }
}
