import { Injectable } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateTodoDto } from '../dto/createTodo.dto';
import { TodoResponseDto } from '../dto/todoResponse.dto';
import { Todo } from '../entities/todo.entity';

@Injectable()
@EntityRepository(Todo)
export class TodoRepository extends Repository<Todo> {
  async createTodo(createTodoDto: CreateTodoDto): Promise<TodoResponseDto> {
    const entity = this.create(createTodoDto);
    const todo = await this.save(entity);
    return buildTodoResponse(todo);
  }
}

function buildTodoResponse(todo: Todo): TodoResponseDto {
  return { ...todo };
}
