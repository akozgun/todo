export interface Configuration {
  database: {
    host: string;
    username: string;
    password: string;
    name: string;
    port: number;
    synchronize: boolean;
  };
  server: {
    port: number;
  };
}
