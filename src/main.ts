import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  configureSwagger(app);
  const configService = app.get(ConfigService);
  await app.listen(configService.get('server.port'));
}
bootstrap();

function configureSwagger(app) {
  const config = new DocumentBuilder()
    .setTitle('Todo App with NestJS')
    .setDescription('An introduction to NestJS for backend compentency.')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('', app, document);
}
