import {
  BaseEntity,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export class AuditableEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @CreateDateColumn()
  dateCreated: Date;
  @UpdateDateColumn()
  dateLastUpdated: Date;
  @Column({ default: false })
  isDeleted: boolean;
}
